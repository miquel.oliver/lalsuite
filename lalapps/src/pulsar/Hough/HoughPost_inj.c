   /* lalapps/hough includes */
    #include "./DriveHoughColor.h"
    #include "./MCInjectHoughMulti.h"

    /* lalapps includes */
    #include <lalapps.h>
    #include "FstatToplist.h"

    /* lal includes */
    #include <lal/DopplerScan.h>
    #include <lal/LogPrintf.h>

    /* gsl includes */
    #include <gsl/gsl_permutation.h>
    #define BLOCKSRNGMED 30 /* Running median window size */

    #define HOUGHMAXFILENAMELENGTH 2048 /* maximum # of characters  of a filename */

    #define DIROUT "./outMulti"   /* output directory */
    #define BASENAMEOUT "HM"    /* prefix file output */

    #define THRESHOLD 1.6 /* thresold for peak selection, with respect to the
    the averaged power in the search band */

    #define TRUE (1==1)
    #define FALSE (1= = 0)

    /* ****************************************
     * Structure, HoughParamsTest, typedef
     */

    typedef struct tagHoughParamsTest{
        UINT4  length;            /* number p of blocks to split the data into */
        UINT4  *numberSFTp;       /* Ni SFTs in each data block */
        REAL8  *sumWeight;        /* Pointer to the sumWeight of each block of data */
        REAL8  *sumWeightSquare;  /* Pointer to the sumWeightSquare of each block of data */
    }HoughParamsTest;


    typedef struct tagUCHARPeakGramVector{
        UINT4             length; /**< number of elements */
        UCHARPeakGram     *upg;    /**< expanded Peakgrams */
    } UCHARPeakGramVector;

    /******************************************/

    void GetAMWeights(LALStatus *status, REAL8Vector *out, MultiDetectorStateSeries *mdetStates, REAL8 alpha, REAL8 delta);

    void ReadTimeStampsFile(LALStatus *status, LIGOTimeGPSVector *ts, CHAR *filename);

    void GetSFTVelTime(LALStatus *status, REAL8Cart3CoorVector *velV, LIGOTimeGPSVector *timeV, MultiDetectorStateSeries *in);

    void GetSFTNoiseWeights(LALStatus *status, REAL8Vector *out, MultiNoiseWeights  *in);

    void GetPeakGramFromMultSFTVector_NondestroyPg1(LALStatus *status, HOUGHPeakGramVector *out, UCHARPeakGramVector *upgV, MultiSFTVector *in, REAL8 thr) ;

    /* double log(double x) */

    int main(int argc, char *argv[]){
        
        /* LALStatus pointer */
        static LALStatus  status;
        FILE *fpToplist = NULL;    
           
        /* time and velocity  */
        LIGOTimeGPSVector    *timeV = NULL;
        REAL8Vector  *timeDiffV = NULL;
        static REAL8Cart3CoorVector velV;
        REAL8Vector *vcProdn = NULL;
        
        LIGOTimeGPS firstTimeStamp, lastTimeStamp;
        REAL8 tObs, tObsTauMin, Freq;

        /* Tau BreakIndex path  */
        REAL8Vector *Taubreak = NULL;
        REAL8Vector *Weight_prod = NULL; 
        /* standard pulsar sft types */
        MultiSFTVector *inputSFTs = NULL;
        /* INT4 numSearchBins;*/
        
        /* information about all the ifos */
        MultiDetectorStateSeries *mdetStates = NULL;
        UINT4 numifo;
        
        /* vector of weights */
        REAL8Vector *weightsV = NULL, *weightsNoise = NULL;

        /* ephemeris */
        EphemerisData    *edat = NULL;
        
        /* hough structures */
        static HOUGHPeakGramVector pgV;  /* vector of peakgrams */
        static UCHARPeakGramVector upgV;  /* vector of expanded peakgrams */
        
        /* miscellaneous nBreakIndexMax, */
        INT4  nBreakMax;

        /* Cos(Delta) position sky, Coherent Time SFTs, Frequency resolution */
        REAL8  cosDelta, timeBase, deltaF;
        REAL8  sourceLocation_x, sourceLocation_y, sourceLocation_z;

        /* Loop index and jumps */
        REAL8  BreakIndexjump, Taujump, Tau, BreakIndex, AT;
        
        /*  Minimum jump for n and Tau */          
        UINT4  sftFminBin, iIFO, iIFOt;

        /*  Number of SFTs */ 
        INT4   mObsCoh,kcount;

        /* Statistical varibles */        
        REAL8  alphaPeak;
        REAL8  meanN, sigmaN,sumWeight, numberCount, sumWeightSquare;
        REAL8Vector  *sumWeightSquare_v= NULL,  *Sensitivity = NULL, *Candidate = NULL ,*sumWeight_mv = NULL, *sumWeightSquare_mv = NULL;

        /* Loop index */
        INT4  iSFT, iTauBreak, i, cand_j, k, j;
        REAL8Vector  *numsft_v= NULL;
        
        /* sft constraint variables */
        LIGOTimeGPS startTimeGPS, endTimeGPS;
        LIGOTimeGPSVector inputTimeStampsVector;
        
        /* user input variables */
        INT4     uvar_blocksRngMed, uvar_maxBinsClean;
        REAL8    uvar_startTime, uvar_endTime;
        REAL8    uvar_f0, uvar_peakThreshold, uvar_freqBand;
        REAL8    uvar_BreakIndex0, uvar_BreakIndexf, uvar_Tau0, uvar_Tauf, uvar_alpha, uvar_delta;
        REAL8    uvar_refTime;
        REAL8    uvar_Tautimes;
        REAL8    uvar_CRth,uvar_CRIFOth;
        REAL8    uvar_fmin, uvar_fmax;        

        CHAR     *uvar_earthEphemeris = NULL;
        CHAR     *uvar_sunEphemeris = NULL;
        CHAR     *uvar_sftData = NULL;
        CHAR     *uvar_dirnameOut = NULL;
        CHAR     *uvar_fbasenameOut = NULL;
        CHAR     *uvar_timeStampsFile = NULL;

        LALStringVector *uvar_linefiles = NULL;
        
        /* LAL error-handler */
        lal_errhandler = LAL_ERR_EXIT;
        uvar_blocksRngMed = BLOCKSRNGMED;
        uvar_alpha = 0;
        uvar_delta = 0;
        uvar_BreakIndex0 = 2;
        uvar_BreakIndexf = 2;
        uvar_Tau0 = 10000;
        uvar_Tauf = 10000;
        uvar_f0 = 1000;
        uvar_freqBand = 10;
        uvar_peakThreshold = THRESHOLD;
        uvar_Tautimes = 0.01;
        uvar_maxBinsClean = 100;
        uvar_CRth = 0;        
        uvar_CRIFOth=0;
        uvar_fmin = 188;      /*   MINIMUM FREQ IN THE CATALOG   */
        uvar_fmax = 2040;    /*   MAX FREQ IN THE CATALOG   */        
        
        uvar_earthEphemeris = (CHAR *)LALCalloc( HOUGHMAXFILENAMELENGTH , sizeof(CHAR));
        uvar_sunEphemeris = (CHAR *)LALCalloc( HOUGHMAXFILENAMELENGTH , sizeof(CHAR));
        
        uvar_dirnameOut = (CHAR *)LALCalloc( HOUGHMAXFILENAMELENGTH , sizeof(CHAR));
        strcpy(uvar_dirnameOut,DIROUT);
        
        uvar_fbasenameOut = (CHAR *)LALCalloc( HOUGHMAXFILENAMELENGTH , sizeof(CHAR));
        strcpy(uvar_fbasenameOut,BASENAMEOUT);

        CHAR *fnameLog=NULL;
        /* Merge signal + noise */
        CHAR       *uvar_sftData_signal = NULL;
        INT4       numsft, binsSFT, iSFTsignalSFT;
        REAL8      h0_add, uvar_hnoise;

        /* standard pulsar sft types */
        MultiSFTVector *signalSFTs = NULL;        
        COMPLEX8   *noiseSFT;
        COMPLEX8   *signalSFT;

        /* input product h0 coeficient */
        uvar_hnoise = 1;

        /* new SFT I/O data types */
        SFTCatalog *catalog_2 = NULL;
        static SFTConstraints constraints_2;

        /* set detector constraint */
        constraints_2.detector = NULL;

        /* register user input variables */
        XLAL_CHECK_MAIN( XLALRegisterNamedUvar( &uvar_f0,                 "f0",                 REAL8,        'f', REQUIRED,  "Start search frequency") == XLAL_SUCCESS, XLAL_EFUNC);
        XLAL_CHECK_MAIN( XLALRegisterNamedUvar( &uvar_freqBand,           "freqBand",           REAL8,        'b', REQUIRED,  "Search frequency band") == XLAL_SUCCESS, XLAL_EFUNC);     
        XLAL_CHECK_MAIN( XLALRegisterNamedUvar( &uvar_BreakIndex0,        "BreakIndex0",        REAL8,        0,   REQUIRED,  "Start search BreakIndex") == XLAL_SUCCESS, XLAL_EFUNC);
        XLAL_CHECK_MAIN( XLALRegisterNamedUvar( &uvar_BreakIndexf,        "Breakf",             REAL8,        0,   REQUIRED,  "Final search BreakIndex") == XLAL_SUCCESS, XLAL_EFUNC);
        XLAL_CHECK_MAIN( XLALRegisterNamedUvar( &uvar_Tau0,               "Tau0",               REAL8,        0,   REQUIRED,  "Start search Tau") == XLAL_SUCCESS, XLAL_EFUNC);
        XLAL_CHECK_MAIN( XLALRegisterNamedUvar( &uvar_Tauf,               "Tauf",               REAL8,        0,   REQUIRED,  "Final search Tau") == XLAL_SUCCESS, XLAL_EFUNC);     
        XLAL_CHECK_MAIN( XLALRegisterNamedUvar( &uvar_alpha,              "Alpha",              REAL8,        0,   REQUIRED,  "Source Location Alpha [rad]") == XLAL_SUCCESS, XLAL_EFUNC);
        XLAL_CHECK_MAIN( XLALRegisterNamedUvar( &uvar_delta,              "Delta",              REAL8,        0,   OPTIONAL,  "Source Location Delta [rad]") == XLAL_SUCCESS, XLAL_EFUNC);       
        XLAL_CHECK_MAIN( XLALRegisterNamedUvar( &uvar_startTime,          "startTime",          REAL8,        0,   OPTIONAL,  "GPS start time of observation") == XLAL_SUCCESS, XLAL_EFUNC);
        XLAL_CHECK_MAIN( XLALRegisterNamedUvar( &uvar_endTime,            "endTime",            REAL8,        0,   OPTIONAL,  "GPS end time of observation") == XLAL_SUCCESS, XLAL_EFUNC);
        XLAL_CHECK_MAIN( XLALRegisterNamedUvar( &uvar_fmin,               "minfreq",            REAL8,        0,   OPTIONAL,  "start frequency") == XLAL_SUCCESS, XLAL_EFUNC);
        XLAL_CHECK_MAIN( XLALRegisterNamedUvar( &uvar_fmax,               "maxfreq",            REAL8,        0,   OPTIONAL,  "end frequency") == XLAL_SUCCESS, XLAL_EFUNC);

        XLAL_CHECK_MAIN( XLALRegisterNamedUvar( &uvar_Tautimes,           "tautimes",           REAL8,        0,   OPTIONAL,  "Number of taus we want our observation time to be") == XLAL_SUCCESS, XLAL_EFUNC);
        XLAL_CHECK_MAIN( XLALRegisterNamedUvar( &uvar_CRth,                "CRth",               REAL8,        0,   OPTIONAL,  "Threshold for CR x IFO") == XLAL_SUCCESS, XLAL_EFUNC);
        XLAL_CHECK_MAIN( XLALRegisterNamedUvar( &uvar_CRIFOth,             "CRIFOth",            REAL8,         0,   OPTIONAL,  "Threshold for CR IFOs combined") == XLAL_SUCCESS, XLAL_EFUNC);

        XLAL_CHECK_MAIN( XLALRegisterNamedUvar( &uvar_peakThreshold,      "peakThreshold",      REAL8,        0,   OPTIONAL,  "Peak selection threshold") == XLAL_SUCCESS, XLAL_EFUNC);
        XLAL_CHECK_MAIN( XLALRegisterNamedUvar( &uvar_earthEphemeris,     "earthEphemeris",     STRING,       'E', OPTIONAL,  "Earth Ephemeris file") == XLAL_SUCCESS, XLAL_EFUNC);
        XLAL_CHECK_MAIN( XLALRegisterNamedUvar( &uvar_sunEphemeris,       "sunEphemeris",       STRING,       'S', OPTIONAL,  "Sun Ephemeris file") == XLAL_SUCCESS, XLAL_EFUNC);
        XLAL_CHECK_MAIN( XLALRegisterNamedUvar( &uvar_sftData,            "sftData",            STRING,       'D', REQUIRED,  "SFT filename pattern noise") == XLAL_SUCCESS, XLAL_EFUNC);

        XLAL_CHECK_MAIN( XLALRegisterNamedUvar( &uvar_dirnameOut,         "dirnameOut",         STRING,       'o', OPTIONAL,  "Output directory") == XLAL_SUCCESS, XLAL_EFUNC);
        XLAL_CHECK_MAIN( XLALRegisterNamedUvar( &uvar_fbasenameOut,       "fbasenameOut",       STRING,       0,   REQUIRED,  "Output file basename") == XLAL_SUCCESS, XLAL_EFUNC);
        XLAL_CHECK_MAIN( XLALRegisterNamedUvar( &uvar_linefiles,          "linefiles",          STRINGVector, 0,   OPTIONAL,  "Comma separated List of linefiles (filenames must contain IFO name)") == XLAL_SUCCESS, XLAL_EFUNC);
        XLAL_CHECK_MAIN( XLALRegisterNamedUvar( &uvar_refTime,            "refTime",            REAL8,        0,   OPTIONAL,  "GPS reference time of observation") == XLAL_SUCCESS, XLAL_EFUNC);    
        XLAL_CHECK_MAIN( XLALRegisterNamedUvar( &uvar_timeStampsFile,     "timeStampsFile",     STRING,       0,   OPTIONAL,  "Input time-stamps file") == XLAL_SUCCESS, XLAL_EFUNC);

        XLAL_CHECK_MAIN( XLALRegisterNamedUvar( &uvar_sftData_signal,     "sftsignal",          STRING,       0,   REQUIRED,  "SFT filename pattern signal") == XLAL_SUCCESS, XLAL_EFUNC);
        XLAL_CHECK_MAIN( XLALRegisterNamedUvar( &uvar_hnoise,             "hnoise",             REAL8,        0,   OPTIONAL,  "hnoise") == XLAL_SUCCESS, XLAL_EFUNC);
        
        /* developer input variables */
        XLAL_CHECK_MAIN( XLALRegisterNamedUvar( &uvar_blocksRngMed,       "blocksRngMed",       INT4,         0,   DEVELOPER, "Running Median block size") == XLAL_SUCCESS, XLAL_EFUNC);
        XLAL_CHECK_MAIN( XLALRegisterNamedUvar( &uvar_maxBinsClean,       "maxBinsClean",       INT4,         0,   DEVELOPER, "Maximum number of bins in cleaning") == XLAL_SUCCESS, XLAL_EFUNC);
        
        
        /* read all command line variables */
        BOOLEAN should_exit = 0;
        XLAL_CHECK_MAIN( XLALUserVarReadAllInput(&should_exit, argc, argv, lalAppsVCSInfoList) == XLAL_SUCCESS, XLAL_EFUNC);
        if (should_exit)
            exit(1);
        
        /* very basic consistency checks on user input */
        if ( uvar_f0 < 0 ) {
            LogPrintf(LOG_CRITICAL, "start frequency must be positive\n");
            exit(1);
        }
        
        if ( uvar_freqBand < 0 ) {
            LogPrintf(LOG_CRITICAL, "search frequency band must be positive\n");
            exit(1);
        }
        
        if ( uvar_peakThreshold < 0 ) {
            LogPrintf(LOG_CRITICAL, "peak selection threshold must be positive\n");
            exit(1);
        }
        
        /* probability of peak selection */
        alphaPeak = exp(-uvar_peakThreshold);
        
        /***** start main calculations *****/

        LogPrintf (LOG_NORMAL, "Reading SFTs noise...");
        /* read sft Files and set up weights */
        /* new SFT I/O data types */
        SFTCatalog *catalog = NULL;
        static SFTConstraints constraints;
        /* set detector constraint */
        constraints.detector = NULL;

        if ( XLALUserVarWasSet( &uvar_startTime ) ) {
            XLALGPSSetREAL8(&startTimeGPS, uvar_startTime);
            constraints.minStartTime = &startTimeGPS;
        }
        if ( XLALUserVarWasSet( &uvar_endTime ) ) {
            XLALGPSSetREAL8(&endTimeGPS, uvar_endTime);
            constraints.maxStartTime = &endTimeGPS;
        }
        if ( XLALUserVarWasSet( &uvar_timeStampsFile ) ) {
            LAL_CALL ( ReadTimeStampsFile ( &status, &inputTimeStampsVector, uvar_timeStampsFile), &status);
            constraints.timestamps = &inputTimeStampsVector;
        }
        /* get sft catalog */
        XLAL_CHECK_MAIN( ( catalog = XLALSFTdataFind( uvar_sftData, &constraints) ) != NULL, XLAL_EFUNC);
        if ( (catalog == NULL) || (catalog->length == 0) ) {
            LogPrintf (LOG_CRITICAL,"Unable to match any SFTs with pattern '%s'\n", uvar_sftData );
            exit(1);
        }
        /* now we can free the inputTimeStampsVector */
        if ( XLALUserVarWasSet( &uvar_timeStampsFile ) ) {
            LALFree( inputTimeStampsVector.data );
        }
        /* read the sfts */
        XLAL_CHECK_MAIN( ( inputSFTs = XLALLoadMultiSFTs ( catalog, uvar_fmin, uvar_fmax) ) != NULL, XLAL_EFUNC);
        numifo = inputSFTs->length;

        LogPrintf (LOG_NORMAL, "...done \n");

        /*******************/
        
        LogPrintf (LOG_NORMAL, "Reading SFTs signal...");
        /* read sft Files and set up weights */

        /* get sft catalog */
        XLAL_CHECK_MAIN( ( catalog_2 = XLALSFTdataFind( uvar_sftData_signal, &constraints_2) ) != NULL, XLAL_EFUNC);
        if ( (catalog_2 == NULL) || (catalog_2 -> length == 0) ) {
            LogPrintf (LOG_CRITICAL,"Unable to match any SFTs with pattern '%s'\n", uvar_sftData_signal );
            exit(1);
        }              
        
        /* now we can free the inputTimeStampsVector */
        if ( XLALUserVarWasSet( &uvar_timeStampsFile ) ) {
            LALFree( inputTimeStampsVector.data );
        }                

        /* read the sfts */
        XLAL_CHECK_MAIN( ( signalSFTs = XLALLoadMultiSFTs ( catalog_2, uvar_fmin, uvar_fmax) ) != NULL, XLAL_EFUNC);              

        LogPrintf (LOG_NORMAL, "...done \n");

        /* find number of sfts */
        /* loop over ifos and calculate number of sfts */
        /* note that we can't use the catalog to determine the number of SFTs 
        because SFTs might be segmented in frequency */
        numsft_v = XLALCreateREAL8Vector(numifo);  
        mObsCoh = 0; /* initialization */
        for (iIFO = 0; iIFO < numifo; iIFO++ ) {
            mObsCoh += inputSFTs->data[iIFO]->length;
            numsft_v->data[iIFO] = inputSFTs->data[iIFO]->length;
        }

        /* first some sft parameters */
        deltaF = catalog->data[0].header.deltaF;  /* frequency resolution */
        timeBase= 1.0/deltaF;                     /* coherent integration time */
        /* numSearchBins = uvar_freqBand * timeBase; total number of search bins - 1 */

        /* catalog is ordered in time so we can get start, end time and tObs*/
        firstTimeStamp = catalog->data[0].header.epoch;
        lastTimeStamp = catalog->data[catalog->length - 1].header.epoch;
        tObs = XLALGPSDiff( &lastTimeStamp, &firstTimeStamp ) + timeBase;

        XLALDestroySFTCatalog(catalog);

        /*  get ephemeris  */
        XLAL_CHECK_MAIN( ( edat = XLALInitBarycenter( uvar_earthEphemeris, uvar_sunEphemeris ) ) != NULL, XLAL_EFUNC);

        /* allocate memory for velocity vector prod*/
        vcProdn = XLALCreateREAL8Vector( mObsCoh);

        /* allocate memory for velocity vector */
        velV.length = mObsCoh;
        velV.data = NULL;
        velV.data = (REAL8Cart3Coor *)LALCalloc(1, mObsCoh*sizeof(REAL8Cart3Coor));

        /* allocate memory for timestamps and timediff vectors */
        timeV = XLALCreateTimestampVector (mObsCoh);
        timeDiffV = XLALCreateREAL8Vector( mObsCoh);

        /* allocate and initialize noise and AMweights vectors */
        weightsV = XLALCreateREAL8Vector( mObsCoh);
        weightsNoise = XLALCreateREAL8Vector( mObsCoh);
        LAL_CALL( LALHOUGHInitializeWeights( &status, weightsNoise), &status);
        LAL_CALL( LALHOUGHInitializeWeights( &status, weightsV), &status);

        const REAL8 tOffset = 0.5 / inputSFTs->data[0]->data[0].deltaF;

        XLAL_CHECK_MAIN ( ( mdetStates = XLALGetMultiDetectorStatesFromMultiSFTs ( inputSFTs, edat, tOffset ) ) != NULL, XLAL_EFUNC);
        LAL_CALL ( GetSFTVelTime( &status, &velV, timeV, mdetStates), &status);
        
        for(j = 0; j < mObsCoh; j++)
            timeDiffV->data[j] = XLALGPSDiff( timeV->data + j, &firstTimeStamp );     

        cosDelta = cos(uvar_delta);
        
        /****  general parameter settings and 1st memory allocation ****/

        sourceLocation_x = cosDelta* cos(uvar_alpha);
        sourceLocation_y = cosDelta* sin(uvar_alpha);
        sourceLocation_z = sin(uvar_delta);

        for (j = 0; j<mObsCoh; ++j){
            vcProdn->data[j] = velV.data[j].x * sourceLocation_x + velV.data[j].y * sourceLocation_y + velV.data[j].z * sourceLocation_z;
        }                 
        
        /* adding signal+ noise SFT, TO BE CHECKED */ 

        binsSFT = inputSFTs->data[0]->data->data->length;        

        h0_add=uvar_hnoise;                    

        /* adding signal+ noise SFT, TO BE CHECKED */ 
        LogPrintf (LOG_NORMAL, "Combine SFTs...");       
        

        for ( k = 0, iIFO = 0; iIFO < numifo; iIFO++){
            numsft = inputSFTs->data[iIFO]->length;

        for ( iSFT = 0; iSFT < numsft; iSFT++, k++) {

                iSFTsignalSFT = floor((timeDiffV->data[k])/timeBase+0.5)-1;

                noiseSFT  = inputSFTs->data[iIFO]->data[iSFT].data->data;

                signalSFT = signalSFTs->data[iIFO]->data[iSFTsignalSFT].data->data;

        for (j = 0; j < binsSFT; j++) {
                *(noiseSFT) = crectf( crealf(*noiseSFT)  + crealf(*signalSFT)*h0_add, cimagf(*noiseSFT)  + cimagf(*signalSFT)*h0_add);
                ++noiseSFT;
                ++signalSFT;
                }
            }
        }

        /* clean sfts if required */
        if ( XLALUserVarWasSet( &uvar_linefiles ) )
        {
            RandomParams *randPar = NULL;
            FILE *fpRand = NULL;
            INT4 seed, ranCount;
            
            LogPrintfVerbatim (LOG_NORMAL, "...cleaning SFTs...");
            if ( (fpRand = fopen("/dev/urandom", "r")) == NULL ) {
                LogPrintf(LOG_CRITICAL,"error in opening /dev/urandom" );
                exit(1);
            }
            
            if ( (ranCount = fread(&seed, sizeof(seed), 1, fpRand)) != 1 ) {
                LogPrintf(LOG_CRITICAL,"error in getting random seed" );
                exit(1);
            }
            
            LAL_CALL ( LALCreateRandomParams (&status, &randPar, seed), &status );
            
            LAL_CALL( LALRemoveKnownLinesInMultiSFTVector ( &status, inputSFTs, uvar_maxBinsClean,
                                                           uvar_blocksRngMed, uvar_linefiles, randPar), &status);
            
            LAL_CALL ( LALDestroyRandomParams (&status, &randPar), &status);
            fclose(fpRand);
        } /* end cleaning */
 
        /* get detector velocities weights vector, and timestamps */
        MultiNoiseWeights *multweight = NULL;
        MultiPSDVector *multPSD = NULL;

        /* normalize sfts */
        XLAL_CHECK_MAIN( ( multPSD = XLALNormalizeMultiSFTVect(  inputSFTs, uvar_blocksRngMed, NULL ) ) != NULL, XLAL_EFUNC);

        /* compute multi noise weights */
        XLAL_CHECK_MAIN ( ( multweight = XLALComputeMultiNoiseWeights ( multPSD, uvar_blocksRngMed, 0) ) != NULL, XLAL_EFUNC);
        
        LAL_CALL ( GetSFTNoiseWeights( &status, weightsNoise, multweight), &status);

        /* we are now done with the psd */
        XLALDestroyMultiNoiseWeights (multweight);        
        XLALDestroyMultiPSDVector  ( multPSD );

        memcpy(weightsV->data, weightsNoise->data, mObsCoh * sizeof(REAL8));

        LogPrintf (LOG_NORMAL, "...done \n");
        LogPrintf (LOG_NORMAL, "Generating peakgrams...");
                
        pgV.length = mObsCoh;
        pgV.pg = NULL;
        pgV.pg = (HOUGHPeakGram *)LALCalloc(1,mObsCoh*sizeof(HOUGHPeakGram));

        
        upgV.length = mObsCoh;
        upgV.upg = NULL;
        upgV.upg = (UCHARPeakGram *)LALCalloc(1,mObsCoh*sizeof(UCHARPeakGram));
        
        LAL_CALL( GetPeakGramFromMultSFTVector_NondestroyPg1(&status, &pgV, &upgV, inputSFTs, uvar_peakThreshold),&status);

        LogPrintf (LOG_NORMAL, "...done \n");

        /* we are done with the sfts and ucharpeakgram now */
        XLALDestroyMultiSFTVector( inputSFTs);

        /* allocate memory for velocity Tau BreakIndex path */
        Candidate = XLALCreateREAL8Vector((4+numifo)*1000);        
        Sensitivity = XLALCreateREAL8Vector( numifo+1);
        sumWeightSquare_v= XLALCreateREAL8Vector(numifo+1);
        Taubreak = XLALCreateREAL8Vector( mObsCoh);
        Weight_prod = XLALCreateREAL8Vector( mObsCoh);
        sumWeight_mv= XLALCreateREAL8Vector(mObsCoh);
        sumWeightSquare_mv= XLALCreateREAL8Vector(mObsCoh);  

        /* calculate amplitude modulation weights if required */
        LAL_CALL( GetAMWeights( &status, weightsV, mdetStates, uvar_alpha, uvar_delta), &status);
        /* Open & erase file to write the toplist */
        fnameLog = (CHAR *)LALCalloc( HOUGHMAXFILENAMELENGTH , sizeof(CHAR));
        strcat(fnameLog, uvar_fbasenameOut);
        strcat(fnameLog,".dat");
        fpToplist = fopen(fnameLog, "w");
        fprintf(fpToplist, "# f0 '%g' freqBand '%g' Tau0 '%g' Tauf '%g' BreakIndex0 '%g' alpha '%g' delta '%g' BreakIndexf '%g' timeBase '%g' CRth '%g' CRIFOth '%g'\n",uvar_f0,uvar_freqBand,uvar_Tau0,uvar_Tauf,uvar_BreakIndex0,uvar_alpha,uvar_delta,uvar_BreakIndexf,timeBase,uvar_CRth,uvar_CRIFOth);

                  
       /* Set-up the step 1 of the grid */                        
        sftFminBin = upgV.upg[0].fminBinIndex;
        /*AT=pow(uvar_Tautimes,(1.0 - uvar_BreakIndex0)/2.0)-1;*/

        AT=uvar_Tautimes;
        BreakIndexjump = (pow((1 + AT),(1/(- 1 + uvar_BreakIndex0))) * pow((-1 + uvar_BreakIndex0),2))/(( uvar_f0 + uvar_freqBand ) * timeBase * log(1.0 + AT));
        Taujump = (pow((1 + AT),((uvar_BreakIndex0/(-1 + uvar_BreakIndex0)))) * (-1 + uvar_BreakIndex0) * uvar_Tau0)/(( uvar_f0 + uvar_freqBand ) * timeBase * AT);          
        nBreakMax = abs(ceil(( uvar_BreakIndexf - uvar_BreakIndex0 ) / BreakIndexjump + 1));

        /*  Initialize loop varibles*/          
        Freq = uvar_f0;
        Tau = uvar_Tau0;        
        BreakIndex = uvar_BreakIndex0;

    cand_j = 0;
    kcount = 0;

    for (i = 0; BreakIndex<=uvar_BreakIndexf; i++)
        {
            LogPrintf(LOG_NORMAL, " BreakIndex '%g' BreakIndexjump '%g' Tau '%g' Taujump '%g' AT '%g' >> '%d'/'%d' <<  '%g' \n", BreakIndex,BreakIndexjump ,Tau,Taujump,AT,i,nBreakMax,abs(ceil(( uvar_Tauf - uvar_Tau0 ) / Taujump + 1)) * uvar_freqBand);                        
            Tau=uvar_Tau0;

            while (Tau <= uvar_Tauf)
            {   

                Freq = uvar_f0;

                    for ( k = 0, iIFO = 0; iIFO < numifo; iIFO++) {
                        sumWeight = 0; sumWeightSquare = 0;
                        for ( iSFT = 0; iSFT < (INT4)numsft_v->data[iIFO]; iSFT++, k++) {

                            /* pre-compute all the paths for n and tau */
                            Taubreak->data[k] = pow(( 1.0 + timeDiffV->data[k] / Tau ),( 1.0 / (1.0 - BreakIndex) ));
                            
                            /* Prepare the weights and do the sumation  */         
                            Weight_prod->data[k] = weightsV->data[k] * Taubreak->data[k] * Taubreak->data[k] * Taubreak->data[k] * Taubreak->data[k];
                            sumWeightSquare += Weight_prod->data[k] * Weight_prod->data[k];
                            sumWeight += Weight_prod->data[k]; 

                            sumWeightSquare_mv->data[k] = sumWeightSquare;                            
                            sumWeight_mv->data[k] = sumWeight;        

                        }
                    }

                    while (Freq <= (uvar_f0 + uvar_freqBand))
                    {
                        tObsTauMin = pow( uvar_fmin / Freq , 1.0 - BreakIndex ) - 1.0; 

                        /* Redefine the observed time to the minimum frequecy and the max observe time acording to n & tau, we are going to evaluate */
                        if (tObsTauMin < AT)
                        {
                            tObs = tObsTauMin * Tau;
                        }
                        else
                        {
                            tObs = AT * Tau;
                        }
                        
                        for ( k = 0, iIFO = 0; iIFO < numifo; iIFO++) {
                            numberCount = 0;  Sensitivity->data[iIFO] = 0; iIFOt = iIFO;
                            if (iIFO==0)
                                iIFOt = 1, Sensitivity->data[0] = uvar_CRth+1;
                            
                            if (Sensitivity->data[iIFOt-1] >= uvar_CRth){ 
                                for ( iSFT = 0; iSFT < (INT4)numsft_v->data[iIFO]; iSFT++, k++) {
                                    if (tObs > (timeDiffV->data[k]+timeBase))
                                    {
                                        /* Find the correct index bin for the corresponding iSFT (time slice)  */  
                                        iTauBreak = floor( (Taubreak->data[k] * Freq * (1.0 + vcProdn->data[k])) * timeBase - sftFminBin + 0.5);   
                                        numberCount += upgV.upg[k].data[iTauBreak] * Weight_prod->data[k];
                                        kcount = k;
                                    }                                            
                                } /* loop over SFTs */
                                meanN = sumWeight_mv->data[kcount] * alphaPeak;
                                sigmaN = sqrt(sumWeightSquare_mv->data[kcount] * alphaPeak * (1.0 - alphaPeak));
                                Sensitivity->data[iIFO] = (numberCount - meanN)/sigmaN;    
                                sumWeightSquare_v->data[iIFO] = sumWeightSquare_mv->data[kcount];   
                            }       
                        } /* loop over IFOs */

                        /* In case of surviving the las veto calculate MultiIFO CR and print after 1000 additions */
                        if (Sensitivity->data[numifo-1] >= uvar_CRth){ 
                            Sensitivity->data[numifo] = 0; sumWeightSquare_v->data[numifo] = 0;
                            for (iIFO = 0; iIFO < numifo; iIFO++){
                                Sensitivity->data[numifo] += Sensitivity->data[iIFO]*sqrt(sumWeightSquare_v->data[iIFO]);
                                sumWeightSquare_v->data[numifo] += sumWeightSquare_v->data[iIFO];  
                            }
                            Sensitivity->data[numifo] = Sensitivity->data[numifo]/sqrt(sumWeightSquare_v->data[numifo]);   
                            if (Sensitivity->data[numifo] >= uvar_CRIFOth){ 

                                Candidate->data[cand_j*6+0] = Freq;
                                Candidate->data[cand_j*6+1] = Tau;
                                Candidate->data[cand_j*6+2] = BreakIndex;
                                for (iIFO = 0; iIFO < (numifo+1); iIFO++)
                                    Candidate->data[cand_j*6+3+iIFO] = Sensitivity->data[iIFO];
                                cand_j++;

                                if (cand_j > 1000){
                                    for (cand_j = 0; cand_j < 1000; cand_j++){
                                        fprintf(fpToplist, "%g %g %g",Candidate->data[cand_j*6+0],Candidate->data[cand_j*6+1],Candidate->data[cand_j*6+2]);
                                        for (iIFO = 0; iIFO < numifo; iIFO++)
                                            fprintf(fpToplist, " %g",Candidate->data[cand_j*6+3+iIFO]);
                                        fprintf(fpToplist, " %g\n",Candidate->data[cand_j*6+3+numifo]);
                                    }
                                    cand_j = 1;
                                }
                            }
                        }

                        Freq += deltaF;
                    }/* loop over freq */

                /* Set-up the next jump on tau */ 
                Taujump = Taujump * (Tau + Taujump) / Tau; 
                Tau += Taujump;                        
            }/* loop over Tau*/    

            /* Set-up the step M of the grid */ 
            BreakIndex += BreakIndexjump;
            Taujump = (pow((1 + AT),((BreakIndex/(-1 + BreakIndex)))) * (-1 + BreakIndex) * uvar_Tau0)/(( uvar_f0 + uvar_freqBand ) * timeBase * AT);
            BreakIndexjump = (pow((1 + AT),(1/(-1 + BreakIndex))) * pow((-1 + BreakIndex),2))/(( uvar_f0 + uvar_freqBand ) * timeBase * log(1.0 + AT));
        }/* loop over BreakIndex */

        if (cand_j > 0){
            for (i = 0; i < cand_j; i++){
                fprintf(fpToplist, "%g %g %g",Candidate->data[i*6+0],Candidate->data[i*6+1],Candidate->data[i*6+2]);
            for (iIFO = 0; iIFO < numifo; iIFO++)
                    fprintf(fpToplist, " %g",Candidate->data[i*6+3+iIFO]);
                fprintf(fpToplist, " %g\n",Candidate->data[i*6+3+numifo]);
            }
        }

        fclose(fpToplist);        

        /*-----------------------------*/
        LogPrintf (LOG_NORMAL, "..done \n");
        /*-----------------------------*/
        
        return status.statusCode;
    }


    /* read timestamps file */
    void ReadTimeStampsFile (LALStatus      *status,
                         LIGOTimeGPSVector  *ts,
                         CHAR               *filename)
    {
        
        FILE  *fp = NULL;
        INT4  numTimeStamps, r;
        UINT4 k;
        REAL8 temp1, temp2;
        
        INITSTATUS(status);
        ATTATCHSTATUSPTR (status);
        
        ASSERT(ts, status, DRIVEHOUGHCOLOR_ENULL,DRIVEHOUGHCOLOR_MSGENULL);
        ASSERT(ts->data == NULL, status, DRIVEHOUGHCOLOR_ENULL,DRIVEHOUGHCOLOR_MSGENULL);
        ASSERT(ts->length == 0, status, DRIVEHOUGHCOLOR_ENULL,DRIVEHOUGHCOLOR_MSGENULL);
        ASSERT(filename, status, DRIVEHOUGHCOLOR_ENULL,DRIVEHOUGHCOLOR_MSGENULL);
        
        if ( (fp = fopen(filename, "r")) == NULL) {
            ABORT( status, DRIVEHOUGHCOLOR_EFILE, DRIVEHOUGHCOLOR_MSGEFILE);
        }
        
        /* count number of timestamps */
        numTimeStamps = 0;
        
        do {
            r = fscanf(fp,"%lf%lf\n", &temp1, &temp2);
            /* make sure the line has the right number of entries or is EOF */
            if (r==2) numTimeStamps++;
        } while ( r != EOF);
        rewind(fp);
        
        ts->length = numTimeStamps;
        ts->data = LALCalloc (1, numTimeStamps * sizeof(LIGOTimeGPS));;
        if ( ts->data == NULL ) {
            fclose(fp);
            ABORT( status, DRIVEHOUGHCOLOR_ENULL, DRIVEHOUGHCOLOR_MSGENULL);
        }
        
        for (k = 0; k < ts->length; k++)
        {
            r = fscanf(fp,"%lf%lf\n", &temp1, &temp2);
            ts->data[k].gpsSeconds = (INT4)temp1;
            ts->data[k].gpsNanoSeconds = (INT4)temp2;
        }
        
        fclose(fp);
        
        DETATCHSTATUSPTR (status);
        /* normal exit */
        RETURN (status);
    }

    void GetSFTVelTime(LALStatus                *status,
                       REAL8Cart3CoorVector     *velV,
                       LIGOTimeGPSVector        *timeV,
                       MultiDetectorStateSeries *in)
    {
        
        UINT4 numifo, numsft, iIFO, iSFT, k;
        
        INITSTATUS(status);
        ATTATCHSTATUSPTR (status);
        
        ASSERT (in, status, DRIVEHOUGHCOLOR_ENULL, DRIVEHOUGHCOLOR_MSGENULL);
        ASSERT (in->length > 0, status, DRIVEHOUGHCOLOR_ENULL, DRIVEHOUGHCOLOR_MSGENULL);
        
        ASSERT (velV, status, DRIVEHOUGHCOLOR_ENULL, DRIVEHOUGHCOLOR_MSGENULL);
        ASSERT (velV->length > 0, status, DRIVEHOUGHCOLOR_ENULL, DRIVEHOUGHCOLOR_MSGENULL);
        ASSERT (velV->data, status, DRIVEHOUGHCOLOR_ENULL, DRIVEHOUGHCOLOR_MSGENULL);
        
        ASSERT (timeV, status, DRIVEHOUGHCOLOR_ENULL, DRIVEHOUGHCOLOR_MSGENULL);
        ASSERT (timeV->length > 0, status, DRIVEHOUGHCOLOR_ENULL, DRIVEHOUGHCOLOR_MSGENULL);
        ASSERT (timeV->data, status, DRIVEHOUGHCOLOR_ENULL, DRIVEHOUGHCOLOR_MSGENULL);
        
        ASSERT (velV->length == timeV->length, status, DRIVEHOUGHCOLOR_EBAD, DRIVEHOUGHCOLOR_MSGEBAD);
        
        numifo = in->length;
        
        /* copy the timestamps, weights, and velocity vector */
        for (k = 0, iIFO = 0; iIFO < numifo; iIFO++ ) {
            
            ASSERT (in->data[iIFO], status, DRIVEHOUGHCOLOR_ENULL, DRIVEHOUGHCOLOR_MSGENULL);
            
            numsft = in->data[iIFO]->length;
            ASSERT (numsft > 0, status, DRIVEHOUGHCOLOR_ENULL, DRIVEHOUGHCOLOR_MSGENULL);
            
            for ( iSFT = 0; iSFT < numsft; iSFT++, k++) {
                
                velV->data[k].x = in->data[iIFO]->data[iSFT].vDetector[0];
                velV->data[k].y = in->data[iIFO]->data[iSFT].vDetector[1];
                velV->data[k].z = in->data[iIFO]->data[iSFT].vDetector[2];
                
                /* mid time of sfts */
                timeV->data[k] = in->data[iIFO]->data[iSFT].tGPS;
                
            } /* loop over SFTs */
            
        } /* loop over IFOs */
        
        DETATCHSTATUSPTR (status);
        
        /* normal exit */
        RETURN (status);
    }

    void GetSFTNoiseWeights(LALStatus          *status,
                            REAL8Vector        *out,
                            MultiNoiseWeights  *in)
    {
        UINT4 numifo, numsft, iIFO, iSFT, k;
        
        INITSTATUS(status);
        ATTATCHSTATUSPTR (status);
        
        ASSERT (in, status, DRIVEHOUGHCOLOR_ENULL, DRIVEHOUGHCOLOR_MSGENULL);
        ASSERT (in->length > 0, status, DRIVEHOUGHCOLOR_ENULL, DRIVEHOUGHCOLOR_MSGENULL);
        
        ASSERT (out, status, DRIVEHOUGHCOLOR_ENULL, DRIVEHOUGHCOLOR_MSGENULL);
        ASSERT (out->length > 0, status, DRIVEHOUGHCOLOR_ENULL, DRIVEHOUGHCOLOR_MSGENULL);
        ASSERT (out->data, status, DRIVEHOUGHCOLOR_ENULL, DRIVEHOUGHCOLOR_MSGENULL);
        
        numifo = in->length;
        
        /* copy the timestamps, weights, and velocity vector */
        for (k = 0, iIFO = 0; iIFO < numifo; iIFO++ ) {
            
            ASSERT (in->data[iIFO], status, DRIVEHOUGHCOLOR_ENULL, DRIVEHOUGHCOLOR_MSGENULL);
            
            numsft = in->data[iIFO]->length;
            ASSERT (numsft > 0, status, DRIVEHOUGHCOLOR_ENULL, DRIVEHOUGHCOLOR_MSGENULL);
            
            for ( iSFT = 0; iSFT < numsft; iSFT++, k++) {
                
                out->data[k] = in->data[iIFO]->data[iSFT];
                
            } /* loop over SFTs */
            
        } /* loop over IFOs */
        
        TRY( LALHOUGHNormalizeWeights( status->statusPtr, out), status);
        
        DETATCHSTATUSPTR (status);
        
        /* normal exit */
        RETURN (status);
    }

void GetAMWeights(LALStatus                *status,
                  REAL8Vector              *out,
                  MultiDetectorStateSeries *mdetStates,
                  REAL8                    alpha,
                  REAL8                    delta)
{
    
    MultiAMCoeffs *multiAMcoef = NULL;
    UINT4 iIFO, iSFT, k, numsft, numifo;
    REAL8 a, b;
    SkyPosition skypos;
    INITSTATUS(status);
    ATTATCHSTATUSPTR (status);

    /* get the amplitude modulation coefficients */
    skypos.longitude = alpha;
    skypos.latitude = delta;
    skypos.system = COORDINATESYSTEM_EQUATORIAL;
    XLAL_CHECK_LAL ( status, ( multiAMcoef = XLALComputeMultiAMCoeffs ( mdetStates, NULL, skypos) ) != NULL, XLAL_EFUNC);
    
    numifo = mdetStates->length;

    /* loop over the weights and multiply them by the appropriate
     AM coefficients */
    for ( k = 0, iIFO = 0; iIFO < numifo; iIFO++) {

        numsft = mdetStates->data[iIFO]->length;
        
        for ( iSFT = 0; iSFT < numsft; iSFT++, k++) {

            a = multiAMcoef->data[iIFO]->a->data[iSFT];
            b = multiAMcoef->data[iIFO]->b->data[iSFT];
            out->data[k] *= (a*a + b*b);
            
        } /* loop over SFTs */
        
    } /* loop over IFOs */
    
    TRY( LALHOUGHNormalizeWeights( status->statusPtr, out), status);
    
    XLALDestroyMultiAMCoeffs ( multiAMcoef );
    
    
    DETATCHSTATUSPTR (status);
    
    /* normal exit */
    RETURN (status);
    
}


/************************************************************************************/
/** Loop over SFTs and set a threshold to get peakgrams.  SFTs must be normalized.  */
/** This function will create a vector with the uncompressed PeakGrams in it        */
/** (this is necesary if we want to compute the chi2 later )                        */
void GetPeakGramFromMultSFTVector_NondestroyPg1(LALStatus                   *status,    /**< pointer to LALStatus structure */
                                                HOUGHPeakGramVector         *out, /**< Output compressed peakgrams */
                                                UCHARPeakGramVector         *upgV, /**< Output uncompressed peakgrams */
                                                MultiSFTVector              *in,  /**< Input SFTs */
                                                REAL8                       thr   /**< Threshold on SFT power */)  
{
    SFTtype  *sft;
    INT4   nPeaks;
    UINT4  iIFO, iSFT, numsft, numifo, j, binsSFT; 
    
    INITSTATUS(status);
    ATTATCHSTATUSPTR (status);
    numifo = in->length;
    binsSFT = in->data[0]->data->data->length;
    
    /* loop over sfts and select peaks */
    for ( j = 0, iIFO = 0; iIFO < numifo; iIFO++){
        numsft = in->data[iIFO]->length;
        
        for ( iSFT = 0; iSFT < numsft; iSFT++, j++) {
            sft = in->data[iIFO]->data + iSFT;
            
            /* Store the expanded PeakGrams */ 
            upgV->upg[j].length = binsSFT;
            upgV->upg[j].data = NULL;
            upgV->upg[j].data= (UCHAR *)LALCalloc( 1, binsSFT*sizeof(UCHAR));
            
            TRY (SFTtoUCHARPeakGram( status->statusPtr, &(upgV->upg[j]), sft, thr), status);
            
            nPeaks = upgV->upg[j].nPeaks;
            
            /* compress peakgram */      
            out->pg[j].length = nPeaks;
            out->pg[j].peak = NULL;
            out->pg[j].peak = (INT4 *)LALCalloc( 1, nPeaks*sizeof(INT4));
            
            TRY( LALUCHAR2HOUGHPeak( status->statusPtr, &(out->pg[j]), &(upgV->upg[j])), status );
            
        } /* loop over SFTs */
    } /* loop over IFOs */
    
    
    
    DETATCHSTATUSPTR (status);
    
    /* normal exit */   
    RETURN (status);
    
}