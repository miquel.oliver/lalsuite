/*
 * Copyright (C) 2006 Reinhard Prix
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with with program; see the file COPYING. If not, write to the
 *  Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 *  MA  02111-1307  USA
 */


/**
 * \author Reinhard Prix
 * \date 2006
 * \file
 * \ingroup lalapps_pulsar_SFTTools
 * \brief Code to convert given input-SFTs (v1 or v2) to v2-SFTs with given extra-comment,
 * and write them out following the SFTv2 naming conventions (see LIGO-T040164-01-Z)
 */

/* ---------- includes ---------- */
#include <sys/stat.h>
#include <sys/types.h>

#include <lalapps.h>

#include <lal/UserInput.h>
#include <lal/PulsarDataTypes.h>
#include <lal/SFTfileIO.h>
#include <lal/SFTutils.h>
#include <lal/LogPrintf.h>


/*---------- DEFINES ----------*/
/*----- Macros ----- */
/*---------- internal types ----------*/

/*---------- empty initializers ---------- */

/*---------- Global variables ----------*/

/* User variables */
typedef struct
{
  CHAR *inputSFTs;
  CHAR *outputDir;
  CHAR *outputSingleSFT;
  CHAR *extraComment;
  CHAR *descriptionMisc;
  CHAR *IFO;
  REAL8 fmin;
  REAL8 fmax;
  REAL8 mysteryFactor;
  INT4 minStartTime;
  INT4 maxStartTime;
  CHAR *timestampsFile;
} UserInput_t;

/* Aded M Matrix */
#include <math.h>           // required for fabsl(), sinl(), cosl()
#include <float.h>          // required for LDBL_EPSILON


/*---------- internal prototypes ----------*/
int initUserVars ( UserInput_t *uvar );
int applyFactor2SFTs ( SFTVector *SFTs, REAL8 factor );
int MixNeighbourSFTs ( SFTVector *SFTs, INT4 p );

void InitMatrixM (INT4 p, REAL8 M[2*p+1][2*p+1]);
double C( double x);

//                         Externally Defined Routines                        //
long double xChebyshev_Tn_Series(long double x, const long double a[], int degree);


//                         Internally Defined Routines                        //
double      Fresnel_Auxiliary_Cosine_Integral( double x );
long double xFresnel_Auxiliary_Cosine_Integral( long double x );

//                         Internally Defined Routines                        //
double      Fresnel_Auxiliary_Sine_Integral( double x );
long double xFresnel_Auxiliary_Sine_Integral( long double x );

static long double Chebyshev_Expansion_0_1(long double x);
static long double Chebyshev_Expansion_1_3(long double x);
static long double Chebyshev_Expansion_3_5(long double x);
static long double Chebyshev_Expansion_5_7(long double x);
static long double Asymptotic_Series( long double x );

//                         Internally Defined Routines                        //
double      Fresnel_Cosine_Integral( double x );
long double xFresnel_Cosine_Integral( long double x );

static long double Power_Series_C( long double x );

//                         Internally Defined Routines                        //
double      Fresnel_Sine_Integral( double x );
long double xFresnel_Sine_Integral( long double x );

static long double Power_Series_S( long double x );

//                         Internally Defined Constants                       //
static long double const sqrt_2pi = 2.506628274631000502415765284811045253006L;


/*==================== FUNCTION DEFINITIONS ====================*/

/*----------------------------------------------------------------------
 * main function
 *----------------------------------------------------------------------*/
int
main(int argc, char *argv[])
{
  SFTConstraints XLAL_INIT_DECL(constraints);
  LIGOTimeGPS XLAL_INIT_DECL(minStartTimeGPS);
  LIGOTimeGPS XLAL_INIT_DECL(maxStartTimeGPS);
  SFTCatalog *FullCatalog = NULL;
  CHAR *add_comment = NULL;
  UINT4 i;
  REAL8 fMin, fMax;




  INT4 p;
  p = 2;





  UserInput_t XLAL_INIT_DECL(uvar);

  /* register all user-variables */
  XLAL_CHECK_MAIN ( initUserVars ( &uvar ) == XLAL_SUCCESS, XLAL_EFUNC );

  /* read cmdline & cfgfile  */
  BOOLEAN should_exit = 0;
  XLAL_CHECK_MAIN( XLALUserVarReadAllInput( &should_exit, argc, argv, lalAppsVCSInfoList ) == XLAL_SUCCESS, XLAL_EFUNC );
  if ( should_exit ) {
    exit (1);
  }

  /* ----- make sure output directory exists ---------- */
  if ( uvar.outputDir )
  {
    int ret;
    ret = mkdir ( uvar.outputDir, 0777);
    if ( (ret == -1) && ( errno != EEXIST ) )
      {
	int errsv = errno;
	LogPrintf (LOG_CRITICAL, "Failed to create directory '%s': %s\n", uvar.outputDir, strerror(errsv) );
	return -1;
      }
  }

  LIGOTimeGPSVector *timestamps = NULL;
  if ( uvar.timestampsFile )
    {
      if ( (timestamps = XLALReadTimestampsFile ( uvar.timestampsFile )) == NULL ) {
        XLALPrintError ("XLALReadTimestampsFile() failed to load timestamps from file '%s'\n", uvar.timestampsFile );
        return -1;
      }
    }

  /* use IFO-contraint if one given by the user */
  if ( XLALUserVarWasSet ( &uvar.IFO ) ) {
    XLAL_CHECK_MAIN ( (constraints.detector = XLALGetChannelPrefix ( uvar.IFO )) != NULL, XLAL_EINVAL );
  }

  minStartTimeGPS.gpsSeconds = uvar.minStartTime;
  maxStartTimeGPS.gpsSeconds = uvar.maxStartTime;
  constraints.minStartTime = &minStartTimeGPS;
  constraints.maxStartTime = &maxStartTimeGPS;
  constraints.timestamps = timestamps;

  /* get full SFT-catalog of all matching (multi-IFO) SFTs */
  XLAL_CHECK_MAIN ( (FullCatalog = XLALSFTdataFind ( uvar.inputSFTs, &constraints )) != NULL, XLAL_EFUNC );

  if ( constraints.detector ) {
    XLALFree ( constraints.detector );
  }

  XLAL_CHECK_MAIN ( (FullCatalog != NULL) && (FullCatalog->length > 0), XLAL_EINVAL, "\nSorry, didn't find any matching SFTs with pattern '%s'!\n\n", uvar.inputSFTs );

  /* build up full comment-string to be added to SFTs: 1) converted by ConvertToSFTv2, VCS ID 2) user extraComment */
  {
    UINT4 len = 128;
    len += strlen ( uvar.inputSFTs );
    if ( uvar.extraComment )
      len += strlen ( uvar.extraComment );

    XLAL_CHECK_MAIN ( ( add_comment = LALCalloc ( 1, len )) != NULL, XLAL_ENOMEM );

    /** \deprecated FIXME: the following code uses obsolete CVS ID tags.
     *  It should be modified to use git version information. */
    sprintf ( add_comment, "Converted by $Id$, inputSFTs = '%s';", uvar.inputSFTs );
    if ( uvar.extraComment )
      {
	strcat ( add_comment, "\n");
	strcat ( add_comment, uvar.extraComment );
      }
  } /* construct comment-string */

  /* which frequency-band to extract? */
  fMin = -1;	/* default: all */
  fMax = -1;
  if ( XLALUserVarWasSet ( &uvar.fmin ) )
    fMin = uvar.fmin;
  if ( XLALUserVarWasSet ( &uvar.fmax ) )
    fMax = uvar.fmax;

  FILE *fpSingleSFT = NULL;
  if ( uvar.outputSingleSFT )
    XLAL_CHECK ( ( fpSingleSFT = fopen ( uvar.outputSingleSFT, "wb" )) != NULL,
                 XLAL_EIO, "Failed to open singleSFT file '%s' for writing\n", uvar.outputSingleSFT );

  /* loop over all SFTs in SFTCatalog */
  for ( i=0; i < FullCatalog->length; i ++ )
    {
      SFTCatalog oneSFTCatalog;
      SFTVector *thisSFT = NULL;
      const CHAR *sft_comment;
      CHAR *new_comment;
      UINT4 comment_len = 0;

      /* set catalog containing only one SFT */
      oneSFTCatalog.length = 1;
      oneSFTCatalog.data = &(FullCatalog->data[i]);

      comment_len = strlen ( add_comment ) + 10;
      sft_comment = oneSFTCatalog.data->comment;
      if ( sft_comment )
	comment_len += strlen ( sft_comment );

      XLAL_CHECK_MAIN ( ( new_comment  = LALCalloc (1, comment_len )) != NULL, XLAL_ENOMEM );

      if ( sft_comment ) {
	strcpy ( new_comment, sft_comment );
	strcat ( new_comment, ";\n");
      }
      strcat ( new_comment, add_comment );

      XLAL_CHECK_MAIN ( (thisSFT = XLALLoadSFTs ( &oneSFTCatalog, fMin, fMax )) != NULL, XLAL_EFUNC );

      if ( uvar.mysteryFactor > 1.0 ) {
	XLAL_CHECK_MAIN ( applyFactor2SFTs ( thisSFT, uvar.mysteryFactor ) == XLAL_SUCCESS, XLAL_EFUNC );
      }

      if ( p > 0 ) { //No mystery factor but p value to build Mixing Matrix. To be fixed
        XLAL_CHECK_MAIN ( MixNeighbourSFTs ( thisSFT, p ) == XLAL_SUCCESS, XLAL_EFUNC );
           }

      // if user asked for single-SFT output, add this SFT to the open file
      if ( uvar.outputSingleSFT )
        XLAL_CHECK ( XLAL_SUCCESS == XLALWriteSFT2fp( &(thisSFT->data[0]), fpSingleSFT, new_comment ),
                     XLAL_EFUNC,  "XLALWriteSFT2fp() failed to write SFT to '%s'!\n", uvar.outputSingleSFT );

      // if user asked for directory output, write this SFT into that directory
      if ( uvar.outputDir ) {
        XLAL_CHECK_MAIN ( XLALWriteSFTVector2Dir ( thisSFT, uvar.outputDir, new_comment, uvar.descriptionMisc ) == XLAL_SUCCESS, XLAL_EFUNC );
      }

      XLALDestroySFTVector ( thisSFT );

      XLALFree ( new_comment );

    } /* for i < numSFTs */

  if ( fpSingleSFT ) {
    fclose ( fpSingleSFT );
  }

  /* free memory */
  XLALFree ( add_comment );
  XLALDestroySFTCatalog ( FullCatalog );
  XLALDestroyUserVars();
  XLALDestroyTimestampVector ( timestamps );

  LALCheckMemoryLeaks();

  return 0;
} /* main */


/*----------------------------------------------------------------------*/
/* register all our "user-variables" */
int
initUserVars ( UserInput_t *uvar )
{
  XLAL_CHECK ( uvar != NULL, XLAL_EINVAL );

  /* set defaults */
  uvar->outputDir = NULL;
  uvar->outputSingleSFT = NULL;

  uvar->extraComment = NULL;
  uvar->descriptionMisc = NULL;
  uvar->IFO = NULL;

  uvar->minStartTime = 0;
  uvar->maxStartTime = LAL_INT4_MAX;

  uvar->mysteryFactor = 1.0;

  uvar->timestampsFile = NULL;

  /* now register all our user-variable */
  XLALRegisterUvarMember( inputSFTs,	STRING, 'i', REQUIRED, "File-pattern for input SFTs");
  XLALRegisterUvarMember( IFO,		STRING, 'I', OPTIONAL, "IFO of input SFTs: 'G1', 'H1', 'H2', ...(required for v1-SFTs)");

  XLALRegisterUvarMember( outputSingleSFT,	STRING, 'O', OPTIONAL, "Output all SFTs into a single concatenated SFT-file with this name");
  XLALRegisterUvarMember( outputDir,	STRING, 'o', OPTIONAL, "Output directory for SFTs");

  XLALRegisterUvarMember( extraComment,	STRING, 'C', OPTIONAL, "Additional comment to be added to output-SFTs");

  XLALRegisterUvarMember( descriptionMisc,	STRING, 'D', OPTIONAL, "'Misc' entry in the SFT filename description-field (see SFTv2 naming convention)");
  XLALRegisterUvarMember(   fmin,		REAL8, 'f', OPTIONAL, "Lowest frequency to extract from SFTs. [Default: lowest in inputSFTs]");
  XLALRegisterUvarMember(   fmax,		REAL8, 'F', OPTIONAL, "Highest frequency to extract from SFTs. [Default: highest in inputSFTs]");


  XLALRegisterUvarMember(  	minStartTime, 	 INT4, 0,  OPTIONAL, "Only use SFTs with timestamps starting from (including) this GPS time");
  XLALRegisterUvarMember(  	maxStartTime, 	 INT4, 0,  OPTIONAL, "Only use SFTs with timestamps up to (excluding) this GPS time");

  XLALRegisterUvarMember( timestampsFile,	 STRING, 0, OPTIONAL, "Timestamps file to use as a constraint for SFT loading");

  /* developer-options */
  XLALRegisterUvarMember(   mysteryFactor,	 REAL8, 0, DEVELOPER, "Change data-normalization by applying this factor (for E@H)");
  

  return XLAL_SUCCESS;

} /* initUserVars() */

int
applyFactor2SFTs ( SFTVector *SFTs, REAL8 factor )
{

  XLAL_CHECK ( SFTs != NULL, XLAL_EINVAL );

  UINT4 numSFTs = SFTs->length;

  for ( UINT4 i=0; i < numSFTs; i ++ )
    {
      SFTtype *thisSFT = &(SFTs->data[i]);
      UINT4 k, numBins = thisSFT->data->length;

      for ( k=0; k < numBins; k ++ )
	{
	  thisSFT->data->data[k] *= ((REAL4) factor);
	} /* for k < numBins */

    } /* for i < numSFTs */

  return XLAL_SUCCESS;

} /* applyFactor2SFTs() */

int
MixNeighbourSFTs ( SFTVector *SFTs, INT4 p )
{

  REAL8 M[2*p+1][2*p+1]; // Matrix M to mix SFTs
  COMPLEX8 counter;
  INT4 i, j;

  /* Construct Mixing matrix M */
  InitMatrixM(p, M);

  XLAL_CHECK ( SFTs != NULL, XLAL_EINVAL );

  UINT4 numSFTs = SFTs->length;

  for ( UINT4 ui=0; ui < numSFTs; ui ++ ) // Loops over SFTS (ui = one SFT)
    {
      SFTtype *thisSFT = &(SFTs->data[ui]);
      UINT4 k, numBins = thisSFT->data->length;
      
      COMPLEX8* sfts_mixed = (COMPLEX8 *) malloc(numBins * sizeof(COMPLEX8)); //Array to mix SFTs

      for ( k=0; k < numBins; k++ ) // Loops over bins (k = one bin)
        {

          if (k <= (UINT4) p || k >= numBins - (UINT4)p + 1){ // Don't mix bins at borders
            sfts_mixed[k] = thisSFT->data->data[k]; 
          } 

          else{ // Mix bins 

            counter = 0;
            for (i=-p; i<= p; i++){
              for (j=-p; j<= p; j++){
                // Careful: Vectors need negative and positive indexes to pick up numbers BEFORE and AFTER the center
                // M matrix only suports indexes from 0 to 2p, which means i+p and j+p indexes must be usead instead of i and j
                counter += conjf(thisSFT->data->data[k+i]) * thisSFT->data->data[k+j] * M[i+p][j+p] ;
              }
            }

            sfts_mixed[k] = counter;
             
              }

        }/* for k < numBins */


      for ( k=0; k < numBins; k++ ) {
      thisSFT->data->data[k] = sfts_mixed[k];
     }
    }/* for i < numSFTs */

  return XLAL_SUCCESS;

} /* applyFactor2SFTs() */


void InitMatrixM (INT4 p, REAL8 M[2*p+1][2*p+1]){
/* Mixing matrix M. It's indexes will run from 0 to 2p, which will be taken into acount when used in MixPSFTs */

  INT4 i, j, ip, jp;
  /* i,j from 0 to 2p. ip, jp from -p to p */
  /* NEED Si, and C(x) = Ci(pi x) - ln(x)*/

  /* Construct non diagonal terms */
  for (i=0 ; i<=2*p; i++){
    for(j=0; j<i; j++){

      ip = i-p;
      jp = j-p;

      M[i][j] = ( C(2*ip-1) - C(2*ip +1) - C(2*jp-1) + C(2*jp+1) ) / (2 * LAL_PI*LAL_PI * (jp - ip));
      M[j][i] = M[i][j];
    }
  }

  /* Diagonal Terms */
  for (i=0; i<= 2*p; i++){
    ip = i-p;
    M[i][i] = 4. /(LAL_PI*LAL_PI * (4*ip*2 - 1)) + Fresnel_Sine_Integral(LAL_PI * (2*ip+1)) / LAL_PI - Fresnel_Sine_Integral(LAL_PI * (2*ip-1)) / LAL_PI;
  }

}


/*----------------------------------------------*/
/* Mathematical Functions                       */



////////////////////////////////////////////////////////////////////////////////
// File: fresnel_sine_integral.c                                              //
// Routine(s):                                                                //
//    Fresnel_Sine_Integral                                                   //
//    xFresnel_Sine_Integral                                                  //
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
//  Note:                                                                     //
//     There are several different definitions of what is called the          //
//     Fresnel sine integral.  The definition of the Fresnel sine integral,   //
//     S(x) programmed below is the integral from 0 to x of the integrand     //
//                          sqrt(2/pi) sin(t^2) dt.                           //
////////////////////////////////////////////////////////////////////////////////

//#include <math.h>           // required for fabsl(), sinl(), cosl()
//#include <float.h>          // required for LDBL_EPSILON



////////////////////////////////////////////////////////////////////////////////
// double Fresnel_Sine_Integral( double x )                                   //
//                                                                            //
//  Description:                                                              //
//     The Fresnel sine integral, S(x), is the integral with integrand        //
//                          sqrt(2/pi) sin(t^2) dt                            //
//     where the integral extends from 0 to x.                                //
//                                                                            //
//  Arguments:                                                                //
//     double  x  The argument of the Fresnel sine integral S().              //
//                                                                            //
//  Return Value:                                                             //
//     The value of the Fresnel sine integral S evaluated at x.               //
//                                                                            //
//  Example:                                                                  //
//     double y, x;                                                           //
//                                                                            //
//     ( code to initialize x )                                               //
//                                                                            //
//     y = Fresnel_Sine_Integral( x );                                        //
////////////////////////////////////////////////////////////////////////////////
double Fresnel_Sine_Integral( double x )
{
   return (double) xFresnel_Sine_Integral( (long double) x);
}


////////////////////////////////////////////////////////////////////////////////
// long double xFresnel_Sine_Integral( long double x )                        //
//                                                                            //
//  Description:                                                              //
//     The Fresnel sine integral, S(x), is the integral with integrand        //
//                          sqrt(2/pi) sin(t^2) dt                            //
//     where the integral extends from 0 to x.                                //
//                                                                            //
//  Arguments:                                                                //
//     long double  x  The argument of the Fresnel sine integral S().         //
//                                                                            //
//  Return Value:                                                             //
//     The value of the Fresnel sine integral S evaluated at x.               //
//                                                                            //
//  Example:                                                                  //
//     long double y, x;                                                      //
//                                                                            //
//     ( code to initialize x )                                               //
//                                                                            //
//     y = xFresnel_Sine_Integral( x );                                       //
////////////////////////////////////////////////////////////////////////////////

long double xFresnel_Sine_Integral( long double x )
{
   long double f;
   long double g;
   long double x2;
   long double s;

   if ( fabsl(x) < 0.5L) return Power_Series_S(x);
   
   f = xFresnel_Auxiliary_Cosine_Integral(fabsl(x));
   g = xFresnel_Auxiliary_Sine_Integral(fabsl(x));
   x2 = x * x;
   s = 0.5L - cosl(x2) * f - sinl(x2) * g;
   return ( x < 0.0L) ? -s : s;
}

////////////////////////////////////////////////////////////////////////////////
// static long double Power_Series_S( long double x )                         //
//                                                                            //
//  Description:                                                              //
//     The power series representation for the Fresnel sine integral, S(x),   //
//      is                                                                    //
//               x^3 sqrt(2/pi) Sum (-x^4)^j / [(4j+3) (2j+1)!]               //
//     where the sum extends over j = 0, ,,,.                                 //
//                                                                            //
//  Arguments:                                                                //
//     long double  x                                                         //
//                The argument of the Fresnel sine integral S().              //
//                                                                            //
//  Return Value:                                                             //
//     The value of the Fresnel sine integral S evaluated at x.               //
//                                                                            //
//  Example:                                                                  //
//     long double y, x;                                                      //
//                                                                            //
//     ( code to initialize x )                                               //
//                                                                            //
//     y = Power_Series_S( x );                                               //
////////////////////////////////////////////////////////////////////////////////

static long double Power_Series_S( long double x )
{ 
   long double x2 = x * x;
   long double x3 = x * x2;
   long double x4 = - x2 * x2;
   long double xn = 1.0L;
   long double Sn = 1.0L;
   long double Sm1 = 0.0L;
   long double term;
   long double factorial = 1.0L;
   long double sqrt_2_o_pi = 7.978845608028653558798921198687637369517e-1L;
   int y = 0;
   
   if (x == 0.0L) return 0.0L;
   Sn /= 3.0L;
   while ( fabsl(Sn - Sm1) > LDBL_EPSILON * fabsl(Sm1) ) {
      Sm1 = Sn;
      y += 1;
      factorial *= (long double)(y + y);
      factorial *= (long double)(y + y + 1);
      xn *= x4;
      term = xn / factorial;
      term /= (long double)(y + y + y + y + 3);
      Sn += term;
   }
   return x3 * sqrt_2_o_pi * Sn;
}


////////////////////////////////////////////////////////////////////////////////
// File: fresnel_cosine_integral.c                                            //
// Routine(s):                                                                //
//    Fresnel_Cosine_Integral                                                 //
//    xFresnel_Cosine_Integral                                                //
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
//  Note:                                                                     //
//     There are several different definitions of what is called the          //
//     Fresnel cosine integral.  The definition of the Fresnel cosine         //
//     integral, C(x) programmed below is the integral from 0 to x of the     //
//     integrand                                                              //
//                          sqrt(2/pi) cos(t^2) dt.                           //
////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////////////
// double Fresnel_Cosine_Integral( double x )                                 //
//                                                                            //
//  Description:                                                              //
//     The Fresnel cosine integral, C(x), is the integral with integrand      //
//                          sqrt(2/pi) cos(t^2) dt                            //
//     where the integral extends from 0 to x.                                //
//                                                                            //
//  Arguments:                                                                //
//     double  x  The argument of the Fresnel cosine integral C().            //
//                                                                            //
//  Return Value:                                                             //
//     The value of the Fresnel cosine integral C evaluated at x.             //
//                                                                            //
//  Example:                                                                  //
//     double y, x;                                                           //
//                                                                            //
//     ( code to initialize x )                                               //
//                                                                            //
//     y = Fresnel_Cosine_Integral( x );                                      //
////////////////////////////////////////////////////////////////////////////////
double Fresnel_Cosine_Integral( double x )
{
   return (double) xFresnel_Cosine_Integral( (long double) x);
}


////////////////////////////////////////////////////////////////////////////////
// long double xFresnel_Cosine_Integral( long double x )                      //
//                                                                            //
//  Description:                                                              //
//     The Fresnel cosine integral, C(x), is the integral with integrand      //
//                          sqrt(2/pi) cos(t^2) dt                            //
//     where the integral extends from 0 to x.                                //
//                                                                            //
//  Arguments:                                                                //
//     long double  x  The argument of the Fresnel cosine integral C().       //
//                                                                            //
//  Return Value:                                                             //
//     The value of the Fresnel cosine integral C evaluated at x.             //
//                                                                            //
//  Example:                                                                  //
//     long double y, x;                                                      //
//                                                                            //
//     ( code to initialize x )                                               //
//                                                                            //
//     y = xFresnel_Cosine_Integral( x );                                     //
////////////////////////////////////////////////////////////////////////////////

long double xFresnel_Cosine_Integral( long double x )
{
   long double f;
   long double g;
   long double x2;
   long double c;
   
   if ( fabsl(x) < 0.5L) return Power_Series_C(x);

   f = xFresnel_Auxiliary_Cosine_Integral(fabsl(x));
   g = xFresnel_Auxiliary_Sine_Integral(fabsl(x));
   x2 = x * x;
   c = 0.5L + sinl(x2) * f - cosl(x2) * g;
   return ( x < 0.0L) ? -c : c;
}


////////////////////////////////////////////////////////////////////////////////
// static long double Power_Series_C( long double x )                         //
//                                                                            //
//  Description:                                                              //
//     The power series representation for the Fresnel cosine integral, C(x), //
//      is                                                                    //
//                 x sqrt(2/pi) Sum (-x^4)^j / [(4j+1) (2j)!]                 //
//     where the sum extends over j = 0, ,,,.                                 //
//                                                                            //
//  Arguments:                                                                //
//     long double  x                                                         //
//                The argument of the Fresnel cosine integral C().            //
//                                                                            //
//  Return Value:                                                             //
//     The value of the Fresnel cosine integral C evaluated at x.             //
//                                                                            //
//  Example:                                                                  //
//     long double y, x;                                                      //
//                                                                            //
//     ( code to initialize x )                                               //
//                                                                            //
//     y = Power_Series_C( x );                                               //
////////////////////////////////////////////////////////////////////////////////

static long double Power_Series_C( long double x )
{ 
   long double x2 = x * x;
   //long double x3 = x * x2;
   long double x4 = - x2 * x2;
   long double xn = 1.0L;
   long double Sn = 1.0L;
   long double Sm1 = 0.0L;
   long double term;
   long double factorial = 1.0L;
   long double sqrt_2_o_pi = 7.978845608028653558798921198687637369517e-1L;
   int y = 0;
   
   if (x == 0.0L) return 0.0L;
   while ( fabsl(Sn - Sm1) > LDBL_EPSILON * fabsl(Sm1) ) {
      Sm1 = Sn;
      y += 1;
      factorial *= (long double)(y + y);
      factorial *= (long double)(y + y - 1);
      xn *= x4;
      term = xn / factorial;
      term /= (long double)(y + y + y + y + 1);
      Sn += term;
   }
   return x * sqrt_2_o_pi * Sn;
}





////////////////////////////////////////////////////////////////////////////////
// File: fresnel_auxiliary_sine_integral.c                                    //
// Routine(s):                                                                //
//    Fresnel_Auxiliary_Sine_Integral                                         //
//    xFresnel_Auxiliary_Sine_Integral                                        //
////////////////////////////////////////////////////////////////////////////////





////////////////////////////////////////////////////////////////////////////////
// double Fresnel_Auxiliary_Sine_Integral( double x )                         //
//                                                                            //
//  Description:                                                              //
//     The Fresnel auxiliary sine integral, g(x), is the integral from 0 to   //
//     infinity of the integrand                                              //
//                     sqrt(2/pi) exp(-2xt) sin(t^2) dt                       //
//     where x >= 0.                                                          //
//                                                                            //
//  Arguments:                                                                //
//     double  x  The argument of the Fresnel auxiliary sine integral g()     //
//                where x >= 0.                                               //
//                                                                            //
//  Return Value:                                                             //
//     The value of the Fresnel auxiliary sine integral g evaluated at        //
//     x >= 0.                                                                //
//                                                                            //
//  Example:                                                                  //
//     double y, x;                                                           //
//                                                                            //
//     ( code to initialize x )                                               //
//                                                                            //
//     y = Fresnel_Auxiliary_Sine_Integral( x );                              //
////////////////////////////////////////////////////////////////////////////////
double Fresnel_Auxiliary_Sine_Integral( double x )
{
   return (double) xFresnel_Auxiliary_Sine_Integral((long double) x);
}


////////////////////////////////////////////////////////////////////////////////
// long double xFresnel_Auxiliary_Sine_Integral( double x )                   //
//                                                                            //
//  Description:                                                              //
//     The Fresnel auxiliary sine integral, g(x), is the integral from 0 to   //
//     infinity of the integrand                                              //
//                     sqrt(2/pi) exp(-2xt) sin(t^2) dt                       //
//     where x >= 0.                                                          //
//                                                                            //
//  Arguments:                                                                //
//     long double  x  The argument of the Fresnel auxiliary sine integral    //
//                     g() where x >= 0.                                      //
//                                                                            //
//  Return Value:                                                             //
//     The value of the Fresnel auxiliary sine integral g evaluated at        //
//     x >= 0.                                                                //
//                                                                            //
//  Example:                                                                  //
//     double y, x;                                                           //
//                                                                            //
//     ( code to initialize x )                                               //
//                                                                            //
//     y = xFresnel_Auxiliary_Sine_Integral( x );                             //
////////////////////////////////////////////////////////////////////////////////
long double xFresnel_Auxiliary_Sine_Integral( long double x )
{
   if (x == 0.0L) return 0.5L;
   if (x <= 1.0L) return Chebyshev_Expansion_0_1(x);
   if (x <= 3.0L) return Chebyshev_Expansion_1_3(x);
   if (x <= 5.0L) return Chebyshev_Expansion_3_5(x);
   if (x <= 7.0L) return Chebyshev_Expansion_5_7(x);
   return Asymptotic_Series( x );
}


////////////////////////////////////////////////////////////////////////////////
// File: fresnel_auxiliary_cosine_integral.c                                  //
// Routine(s):                                                                //
//    Fresnel_Auxiliary_Cosine_Integral                                       //
//    xFresnel_Auxiliary_Cosine_Integral                                      //
////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////////////
// double Fresnel_Auxiliary_Cosine_Integral( double x )                       //
//                                                                            //
//  Description:                                                              //
//     The Fresnel auxiliary cosine integral, f(x), is the integral from 0 to //
//     infinity of the integrand                                              //
//                     sqrt(2/pi) exp(-2xt) cos(t^2) dt                       //
//     where x >= 0.                                                          //
//                                                                            //
//  Arguments:                                                                //
//     double  x  The argument of the Fresnel auxiliary cosine integral f()   //
//                where x >= 0.                                               //
//                                                                            //
//  Return Value:                                                             //
//     The value of the Fresnel auxiliary cosine integral f evaluated at      //
//     x >= 0.                                                                //
//                                                                            //
//  Example:                                                                  //
//     double y, x;                                                           //
//                                                                            //
//     ( code to initialize x )                                               //
//                                                                            //
//     y = Fresnel_Auxiliary_Cosine_Integral( x );                            //
////////////////////////////////////////////////////////////////////////////////
double Fresnel_Auxiliary_Cosine_Integral( double x )
{
   return (double) xFresnel_Auxiliary_Cosine_Integral((long double) x);
}


////////////////////////////////////////////////////////////////////////////////
// long double xFresnel_Auxiliary_Cosine_Integral( double x )                 //
//                                                                            //
//  Description:                                                              //
//     The Fresnel auxiliary cosine integral, f(x), is the integral from 0 to //
//     infinity of the integrand                                              //
//                     sqrt(2/pi) exp(-2xt) cos(t^2) dt                       //
//     where x >= 0.                                                          //
//                                                                            //
//  Arguments:                                                                //
//     long double  x  The argument of the Fresnel auxiliary cosine integral  //
//                     f() where x >= 0.                                      //
//                                                                            //
//  Return Value:                                                             //
//     The value of the Fresnel auxiliary cosine integral f evaluated at      //
//     x >= 0.                                                                //
//                                                                            //
//  Example:                                                                  //
//     double y, x;                                                           //
//                                                                            //
//     ( code to initialize x )                                               //
//                                                                            //
//     y = xFresnel_Auxiliary_Cosine_Integral( x );                           //
////////////////////////////////////////////////////////////////////////////////
long double xFresnel_Auxiliary_Cosine_Integral( long double x )
{
   if (x == 0.0L) return 0.5L;
   if (x <= 1.0L) return Chebyshev_Expansion_0_1(x);
   if (x <= 3.0L) return Chebyshev_Expansion_1_3(x);
   if (x <= 5.0L) return Chebyshev_Expansion_3_5(x);
   if (x <= 7.0L) return Chebyshev_Expansion_5_7(x);
   return Asymptotic_Series( x );
}


////////////////////////////////////////////////////////////////////////////////
// static long double Chebyshev_Expansion_0_1( long double x )                //
//                                                                            //
//  Description:                                                              //
//     Evaluate the Fresnel auxiliary cosine integral, f(x), on the interval  //
//     0 < x <= 1 using the Chebyshev interpolation formula.                  //
//                                                                            //
//  Arguments:                                                                //
//     long double  x  The argument of the Fresnel auxiliary cosine integral  //
//                     where 0 < x <= 1.                                      //
//                                                                            //
//  Return Value:                                                             //
//     The value of the Fresnel auxiliary cosine integral f evaluated at      //
//     x where 0 < x <= 1.                                                    //
//                                                                            //
//  Example:                                                                  //
//     long double y, x;                                                      //
//                                                                            //
//     ( code to initialize x )                                               //
//                                                                            //
//     y = Chebyshev_Expansion_0_1(x);                                        //
////////////////////////////////////////////////////////////////////////////////

static long double Chebyshev_Expansion_0_1( long double x )
{ 
   static long double const c[] = {
      +4.200987560240514577713e-1L,  -9.358785913634965235904e-2L,
      -7.642539415723373644927e-3L,  +4.958117751796130135544e-3L,
      -9.750236036106120253456e-4L,  +1.075201474958704192865e-4L,
      -4.415344769301324238886e-6L,  -7.861633919783064216022e-7L,
      +1.919240966215861471754e-7L,  -2.175775608982741065385e-8L,
      +1.296559541430849437217e-9L,  +2.207205095025162212169e-11L,
      -1.479219615873704298874e-11L, +1.821350127295808288614e-12L,
      -1.228919312990171362342e-13L, +2.227139250593818235212e-15L,
      +5.734729405928016301596e-16L, -8.284965573075354177016e-17L,
      +6.067422701530157308321e-18L, -1.994908519477689596319e-19L,
      -1.173365630675305693390e-20L 
   };

   static const int degree = sizeof(c) / sizeof(long double) - 1;
   static const long double midpoint = 0.5L;
   static const long double scale = 0.5L;
   
   return xChebyshev_Tn_Series( (x - midpoint) / scale, c, degree );
}


////////////////////////////////////////////////////////////////////////////////
// static long double Chebyshev_Expansion_1_3( long double x )                //
//                                                                            //
//  Description:                                                              //
//     Evaluate the Fresnel auxiliary cosine integral, f(x), on the interval  //
//     1 < x <= 3 using the Chebyshev interpolation formula.                  //
//                                                                            //
//  Arguments:                                                                //
//     long double  x  The argument of the Fresnel auxiliary cosine integral  //
//                     where 1 < x <= 3.                                      //
//                                                                            //
//  Return Value:                                                             //
//     The value of the Fresnel auxiliary cosine integral f evaluated at      //
//     x where 1 < x <= 3.                                                    //
//                                                                            //
//  Example:                                                                  //
//     long double y, x;                                                      //
//                                                                            //
//     ( code to initialize x )                                               //
//                                                                            //
//     y = Chebyshev_Expansion_1_3(x);                                        //
////////////////////////////////////////////////////////////////////////////////

static long double Chebyshev_Expansion_1_3( long double x )
{ 
   static long double const c[] = {
      +2.098677278318224971989e-1L,  -9.314234883154103266195e-2L,
      +1.739905936938124979297e-2L,  -2.454274824644285136137e-3L,
      +1.589872606981337312438e-4L,  +4.203943842506079780413e-5L,
      -2.018022256093216535093e-5L,  +5.125709636776428285284e-6L,
      -9.601813551752718650057e-7L,  +1.373989484857155846826e-7L,
      -1.348105546577211255591e-8L,  +2.745868700337953872632e-10L,
      +2.401655517097260106976e-10L, -6.678059547527685587692e-11L,
      +1.140562171732840809159e-11L, -1.401526517205212219089e-12L,
      +1.105498827380224475667e-13L, +2.040731455126809208066e-16L,
      -1.946040679213045143184e-15L, +4.151821375667161733612e-16L,
      -5.642257647205149369594e-17L, +5.266176626521504829010e-18L,
      -2.299025577897146333791e-19L, -2.952226367506641078731e-20L,
      +8.760405943193778149078e-21L
   };

   static const int degree = sizeof(c) / sizeof(long double) - 1;
   static const long double midpoint = 2.0L;
   
   return xChebyshev_Tn_Series( (x - midpoint), c, degree );

}


////////////////////////////////////////////////////////////////////////////////
// static long double Chebyshev_Expansion_3_5( long double x )                //
//                                                                            //
//  Description:                                                              //
//     Evaluate the Fresnel auxiliary cosine integral, g(x), on the interval  //
//     3 < x <= 5 using the Chebyshev interpolation formula.                  //
//                                                                            //
//  Arguments:                                                                //
//     long double  x  The argument of the Fresnel auxiliary cosine integral  //
//                     where 3 < x <= 5.                                      //
//                                                                            //
//  Return Value:                                                             //
//     The value of the Fresnel auxiliary cosine integral f evaluated at      //
//     x where 3 < x <= 5.                                                    //
//                                                                            //
//  Example:                                                                  //
//     long double y, x;                                                      //
//                                                                            //
//     ( code to initialize x )                                               //
//                                                                            //
//     y = Chebyshev_Expansion_3_5(x);                                        //
////////////////////////////////////////////////////////////////////////////////

static long double Chebyshev_Expansion_3_5( long double x )
{ 
   static long double const c[] = {
      +1.025703371090289562388e-1L,  -2.569833023232301400495e-2L,
      +3.160592981728234288078e-3L,  -3.776110718882714758799e-4L,
      +4.325593433537248833341e-5L,  -4.668447489229591855730e-6L,
      +4.619254757356785108280e-7L,  -3.970436510433553795244e-8L,
      +2.535664754977344448598e-9L,  -2.108170964644819803367e-11L,
      -2.959172018518707683013e-11L, +6.727219944906606516055e-12L,
      -1.062829587519902899001e-12L, +1.402071724705287701110e-13L,
      -1.619154679722651005075e-14L, +1.651319588396970446858e-15L,
      -1.461704569438083772889e-16L, +1.053521559559583268504e-17L,
      -4.760946403462515858756e-19L, -1.803784084922403924313e-20L,
      +7.873130866418738207547e-21L
   };

   static const int degree = sizeof(c) / sizeof(long double) - 1;
   static const long double midpoint = 4.0L;
   
   return xChebyshev_Tn_Series( (x - midpoint), c, degree );
}


////////////////////////////////////////////////////////////////////////////////
// static long double Chebyshev_Expansion_5_7( long double x )                //
//                                                                            //
//  Description:                                                              //
//     Evaluate the Fresnel auxiliary cosine integral, g(x), on the interval  //
//     5 < x <= 7 using the Chebyshev interpolation formula.                  //
//                                                                            //
//  Arguments:                                                                //
//     long double  x  The argument of the Fresnel auxiliary cosine integral  //
//                     where 5 < x <= 7.                                      //
//                                                                            //
//  Return Value:                                                             //
//     The value of the Fresnel auxiliary cosine integral f evaluated at      //
//     x where 5 < x <= 7.                                                    //
//                                                                            //
//  Example:                                                                  //
//     long double y, x;                                                      //
//                                                                            //
//     ( code to initialize x )                                               //
//                                                                            //
//     y = Chebyshev_Expansion_5_7(x);                                        //
////////////////////////////////////////////////////////////////////////////////

static long double Chebyshev_Expansion_5_7( long double x )
{ 
   static long double const c[] = {
      +6.738667333400589274018e-2L,  -1.128146832637904868638e-2L,
      +9.408843234170404670278e-4L,  -7.800074103496165011747e-5L,
      +6.409101169623350885527e-6L,  -5.201350558247239981834e-7L,
      +4.151668914650221476906e-8L,  -3.242202015335530552721e-9L,
      +2.460339340900396789789e-10L, -1.796823324763304661865e-11L,
      +1.244108496436438952425e-12L, -7.950417122987063540635e-14L,
      +4.419142625999150971878e-15L, -1.759082736751040110146e-16L,
      -1.307443936270786700760e-18L, +1.362484141039320395814e-18L,
      -2.055236564763877250559e-19L, +2.329142055084791308691e-20L,
      -2.282438671525884861970e-21L
   };

   static const int degree = sizeof(c) / sizeof(long double) - 1;
   static const long double midpoint = 6.0L;
   
   return xChebyshev_Tn_Series( (x - midpoint), c, degree );

}


////////////////////////////////////////////////////////////////////////////////
// static long double Asymptotic_Series( long double x )                      //
//                                                                            //
//  Description:                                                              //
//     For a large argument x, the auxiliary Fresnel cosine integral, f(x),   //
//     can be expressed as the asymptotic series                              //
//      f(x) ~ 1/(x*sqrt(2pi))[1 - 3/4x^4 + 105/16x^8 + ... +                 //
//                                                (4j-1)!!/(-4x^4)^j + ... ]  //
//                                                                            //
//  Arguments:                                                                //
//     long double  x  The argument of the Fresnel auxiliary cosine integral  //
//                     where x > 7.                                           //
//                                                                            //
//  Return Value:                                                             //
//     The value of the Fresnel auxiliary cosine integral f evaluated at      //
//     x where x > 7.                                                         //
//                                                                            //
//  Example:                                                                  //
//     long double y, x;                                                      //
//                                                                            //
//     ( code to initialize x )                                               //
//                                                                            //
//     y = Asymptotic_Series( x );                                            //
////////////////////////////////////////////////////////////////////////////////
#define NUM_ASYMPTOTIC_TERMS 35
static long double Asymptotic_Series( long double x )
{
   long double x2 = x * x;
   long double x4 = -4.0L * x2 * x2;
   long double xn = 1.0L;
   long double factorial = 1.0L;
   long double f = 0.0L;
   long double term[NUM_ASYMPTOTIC_TERMS + 1];
   long double epsilon = LDBL_EPSILON / 4.0L;
   int j = 3;
   int i = 0;

   term[0] = 1.0L;
   term[NUM_ASYMPTOTIC_TERMS] = 0.0L;
   for (i = 1; i < NUM_ASYMPTOTIC_TERMS; i++) {
      factorial *= ( (long double)j * (long double)(j - 2));
      xn *= x4;
      term[i] = factorial / xn;
      j += 4;
      if (fabsl(term[i]) >= fabsl(term[i-1])) {
         i--;
         break;
      }
      if (fabsl(term[i]) <= epsilon) break;
   }
   
   for (; i >= 0; i--) f += term[i];

   return f / (x * sqrt_2pi);   
}

////////////////////////////////////////////////////////////////////////////////
// File: xchebyshev_Tn_series.c                                               //
// Routine(s):                                                                //
//    xChebyshev_Tn_Series                                                    //
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// long double xChebyshev_Tn_Series(long double x, long double a[],int degree)//
//                                                                            //
//  Description:                                                              //
//     This routine uses Clenshaw's recursion algorithm to evaluate a given   //
//     polynomial p(x) expressed as a linear combination of Chebyshev         //
//     polynomials of the first kind, Tn, at a point x,                       //
//       p(x) = a[0] + a[1]*T[1](x) + a[2]*T[2](x) + ... + a[deg]*T[deg](x).  //
//                                                                            //
//     Clenshaw's recursion formula applied to Chebyshev polynomials of the   //
//     first kind is:                                                         //
//     Set y[degree + 2] = 0, y[degree + 1] = 0, then for k = degree, ..., 1  //
//     set y[k] = 2 * x * y[k+1] - y[k+2] + a[k].  Finally                    //
//     set y[0] = x * y[1] - y[2] + a[0].  Then p(x) = y[0].                  //
//                                                                            //
//  Arguments:                                                                //
//     long double x                                                          //
//        The point at which to evaluate the polynomial.                      //
//     long double a[]                                                        //
//        The coefficients of the expansion in terms of Chebyshev polynomials,//
//        i.e. a[k] is the coefficient of T[k](x).  Note that in the calling  //
//        routine a must be defined double a[N] where N >= degree + 1.        //
//     int    degree                                                          //
//        The degree of the polynomial p(x).                                  //
//                                                                            //
//  Return Value:                                                             //
//     The value of the polynomial at x.                                      //
//     If degree is negative, then 0.0 is returned.                           //
//                                                                            //
//  Example:                                                                  //
//     long double x, a[N], p;                                                //
//     int    deg = N - 1;                                                    //
//                                                                            //
//     ( code to initialize x, and a[i] i = 0, ... , a[deg] )                 //
//                                                                            //
//     p = xChebyshev_Tn_Series(x, a, deg);                                   //
////////////////////////////////////////////////////////////////////////////////

long double xChebyshev_Tn_Series(long double x, const long double a[], int degree)
{
   long double yp2 = 0.0L;
   long double yp1 = 0.0L;
   long double y = 0.0L;
   long double two_x = x + x;
   int k;

             // Check that degree >= 0.  If not, then return 0. //

   if ( degree < 0 ) return 0.0L;

           // Apply Clenshaw's recursion save the last iteration. //
 
   for (k = degree; k >= 1; k--, yp2 = yp1, yp1 = y) 
      y = two_x * yp1 - yp2 + a[k];

           // Now apply the last iteration and return the result. //

   return x * yp1 - yp2 + a[0];
}

double C(double x)
{

  return Fresnel_Cosine_Integral(x) - log(x);

}